from euler import edo
import numpy as np
import sympy as sc

if __name__ == "__main__":
    print("Bienvenido! \nProcesando...")
    a = 0
    b = 4
    n = 1000 #Mientras mas grande sea este número mejor se aproximara Euler a Analitica
    y0 = 1
    punto = 10
    f = lambda x,y: 5*y/(x+1)
    soledo = edo(a,b,n,y0,f,punto)
    soledo.figPlot()
    a,b = soledo.euler()
    print("intervalo x = ({} , {}) ".format(soledo.a,soledo.b))
    print("ecuacion -> {}".format(soledo.equation()))
    #print("x = {}".format(soledo.euler()))
    


